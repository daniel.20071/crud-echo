package router

import (
	"database/sql"

	"cloud.google.com/go/storage"
	"github.com/kuma-coffee/crud-echo/pkg/controller"
	"github.com/kuma-coffee/crud-echo/pkg/repository"
	"github.com/kuma-coffee/crud-echo/pkg/usecase"
	"github.com/labstack/echo/v4"
)

func NewUserRouter(e *echo.Echo, g *echo.Group, db *sql.DB, storeRef *storage.BucketHandle) {
	ur := repository.NewUserRepository(db)
	uu := usecase.NewUserUsecase(ur, storeRef)
	uc := &controller.UserController{
		UserUsecase: uu,
	}

	g.GET("/user", uc.GetUsers)
	e.POST("/user/login", uc.LoginUser)
	g.GET("/user/:id", uc.GetUserById)
	e.POST("/user", uc.PostUser)
	g.PUT("/user/:id", uc.UpdateUser)
	g.DELETE("/user/:id", uc.DeleteUser)
	e.GET("/user/download", uc.DownloadUserImageProfile)
}

package usecase

import (
	"context"
	"errors"
	"fmt"
	"io"
	"mime/multipart"

	"cloud.google.com/go/storage"
	"github.com/kuma-coffee/crud-echo/config"
	"github.com/kuma-coffee/crud-echo/pkg/domain"
	"github.com/kuma-coffee/crud-echo/pkg/dto"
	"github.com/kuma-coffee/crud-echo/shared/util"
	"github.com/mitchellh/mapstructure"
)

type UserUsecase struct {
	UserRepository domain.UserRepository
	storeRef       *storage.BucketHandle
}

func NewUserUsecase(UserRepository domain.UserRepository, storeRef *storage.BucketHandle) domain.UserUsecase {
	return &UserUsecase{UserRepository, storeRef}
}

func (uu UserUsecase) GetUsers() ([]domain.User, error) {
	return uu.UserRepository.GetUsers()
}

func (uu UserUsecase) GetUserById(id int) (domain.User, error) {
	return uu.UserRepository.GetUserById(id)
}

func (uu UserUsecase) PostUser(userDTO dto.UserDTO, src multipart.File) error {
	var user domain.User
	mapstructure.Decode(userDTO, &user)

	// passwordHash, err := helper.HashPassword(user.Password)
	// if err != nil {
	// 	return err
	// }
	// user.Password = passwordHash

	user.Password = util.EncryptPassword(user.Password)

	err := uu.UploadUserImageProfile(src, user.Username)
	if err != nil {
		return err
	}

	return uu.UserRepository.PostUser(user)
}

func (uu UserUsecase) UpdateUser(id int, userDTO dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(userDTO, &user)

	// passwordHash, err := helper.HashPassword(user.Password)
	// if err != nil {
	// 	return err
	// }

	// user.Password = passwordHash

	user.Password = util.EncryptPassword(user.Password)

	return uu.UserRepository.UpdateUser(id, user)
}

func (uu UserUsecase) DeleteUser(id int) error {
	return uu.UserRepository.DeleteUser(id)
}

func (uu UserUsecase) GetUserEmail(email string) (domain.User, error) {
	return uu.UserRepository.GetUserEmail(email)
}

func (uu UserUsecase) UserLogin(req dto.LoginRequestDTO) (interface{}, error) {
	loginResponse := dto.LoginResponse{}

	user, err := uu.UserRepository.GetUserEmail(req.Email)
	if err != nil {
		return nil, errors.New("email not found")
	}

	passwordValid := util.DecryptPassword(user.Password)
	if passwordValid != req.Password {
		return nil, errors.New("bad credential")
	}

	token, err := util.CreateJwtToken(user)
	if err != nil {
		return nil, err
	}

	mapstructure.Decode(user, &loginResponse)
	loginResponse.Token = token

	return loginResponse, nil
}

func (uu UserUsecase) UploadUserImageProfile(src multipart.File, username string) error {
	dst := fmt.Sprintf("user-profile/%s", username)

	wc := uu.storeRef.Object(dst).NewWriter(context.Background())
	_, err := io.Copy(wc, src)
	if err != nil {
		return err
	}
	err = wc.Close()
	if err != nil {
		return err
	}

	return nil
}

func (uu UserUsecase) DownloadUserImageProfile(username string) (string, error) {
	conf := config.GetConfig()

	link := fmt.Sprintf("https://firebasestorage.googleapis.com/v0/b/%s/o/user-profile%s%s?alt=media", conf.ClientBucket, "%2F", username)

	return link, nil
}

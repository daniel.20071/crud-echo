package repository

import (
	"database/sql"

	"github.com/kuma-coffee/crud-echo/pkg/domain"
)

type UserRepository struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) domain.UserRepository {
	return &UserRepository{db}
}

func (ur UserRepository) GetUsers() ([]domain.User, error) {
	users := []domain.User{}

	sql := `SELECT * FROM users ORDER BY user_id ASC`

	rows, err := ur.db.Query(sql)
	for rows.Next() {
		user := domain.User{}

		err := rows.Scan(&user.Id, &user.Username, &user.Email, &user.Address, &user.Password)
		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	return users, err
}

func (ur UserRepository) GetUserById(id int) (domain.User, error) {
	user := domain.User{}

	sql := `SELECT * FROM userss WHERE user_id = $1`

	err := ur.db.QueryRow(sql, id).Scan(&user.Id, &user.Username, &user.Email, &user.Address, &user.Password)

	return user, err
}

func (ur UserRepository) PostUser(user domain.User) error {
	sql := `INSERT INTO userss(username, email, address, password, img_profile) VALUES($1, $2, $3, $4, $5)`

	_, err := ur.db.Exec(sql, user.Username, user.Email, user.Address, user.Password, user.ImgProfile)

	return err
}

func (ur UserRepository) UpdateUser(id int, user domain.User) error {
	sql := `UPDATE userss SET username = $2, email = $3, address = $4, password = $5 WHERE user_id = $1`

	_, err := ur.db.Exec(sql, id, user.Username, user.Email, user.Address, user.Password)

	return err
}

func (ur UserRepository) DeleteUser(id int) error {
	sql := `DELETE FROM userss WHERE user_id = $1`

	_, err := ur.db.Exec(sql, id)

	return err
}

func (ur UserRepository) GetUserEmail(email string) (domain.User, error) {
	user := domain.User{}

	sql := `SELECT * FROM userss WHERE email = $1`

	err := ur.db.QueryRow(sql, email).Scan(&user.Id, &user.Username, &user.Email, &user.Address, &user.Password)
	return user, err
}

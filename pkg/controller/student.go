package controller

import (
	"net/http"
	"strconv"

	"github.com/kuma-coffee/crud-echo/pkg/domain"
	"github.com/kuma-coffee/crud-echo/pkg/dto"
	"github.com/kuma-coffee/crud-echo/shared/util"
	"github.com/labstack/echo/v4"
)

type StudentController struct {
	StudentUsecase domain.StudentUsecase
}

func (sc *StudentController) GetStudents(c echo.Context) error {
	resp, err := sc.StudentUsecase.GetStudents()
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success get students", resp)
}

func (sc *StudentController) GetStudentById(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	resp, err := sc.StudentUsecase.GetStudentById(id)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success get student", resp)
}

func (sc *StudentController) PostStudent(c echo.Context) error {
	var studentDTO dto.StudentDTO

	err := c.Bind(&studentDTO)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = studentDTO.Validation()
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = sc.StudentUsecase.PostStudent(studentDTO)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusCreated, "success create student", nil)
}

func (sc *StudentController) UpdateStudent(c echo.Context) error {
	var studentDTO dto.StudentDTO

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = c.Bind(&studentDTO)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	_, err = sc.StudentUsecase.GetStudentById(id)
	if err != nil {
		return util.SetResponse(c, http.StatusNotFound, "student id not found", nil)
	}

	err = studentDTO.Validation()
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = sc.StudentUsecase.UpdateStudent(id, studentDTO)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success update student", nil)
}

func (sc *StudentController) DeleteStudent(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	_, err = sc.StudentUsecase.GetStudentById(id)
	if err != nil {
		return util.SetResponse(c, http.StatusNotFound, "student id not found", nil)
	}

	err = sc.StudentUsecase.DeleteStudent(id)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success delete student", nil)
}

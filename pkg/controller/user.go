package controller

import (
	"net/http"
	"strconv"

	"github.com/kuma-coffee/crud-echo/pkg/domain"
	"github.com/kuma-coffee/crud-echo/pkg/dto"
	"github.com/kuma-coffee/crud-echo/shared/util"
	"github.com/labstack/echo/v4"
)

type UserController struct {
	UserUsecase domain.UserUsecase
}

func (uc *UserController) GetUsers(c echo.Context) error {
	resp, err := uc.UserUsecase.GetUsers()
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success get users", resp)
}

func (uc *UserController) GetUserById(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	resp, err := uc.UserUsecase.GetUserById(id)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success get user", resp)
}

func (uc *UserController) PostUser(c echo.Context) error {
	file, err := c.FormFile("file")
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	src, err := file.Open()
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	userDTO := dto.UserDTO{
		Username:   c.FormValue("username"),
		Email:      c.FormValue("email"),
		Address:    c.FormValue("address"),
		Password:   c.FormValue("password"),
		ImgProfile: file.Filename,
	}

	err = userDTO.Validation()
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	_, err = uc.UserUsecase.GetUserEmail(userDTO.Email)
	if err == nil {
		return util.SetResponse(c, http.StatusNotFound, "user email already exists", nil)
	}

	err = uc.UserUsecase.PostUser(userDTO, src)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusCreated, "success create user", nil)
}

func (uc *UserController) UpdateUser(c echo.Context) error {
	var userDTO dto.UserDTO

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = c.Bind(&userDTO)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = userDTO.Validation()
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	_, err = uc.UserUsecase.GetUserById(id)
	if err != nil {
		return util.SetResponse(c, http.StatusNotFound, "user id not found", nil)
	}

	err = uc.UserUsecase.UpdateUser(id, userDTO)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success update user", nil)
}

func (uc *UserController) DeleteUser(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	_, err = uc.UserUsecase.GetUserById(id)
	if err != nil {
		return util.SetResponse(c, http.StatusNotFound, "user id not found", nil)
	}

	err = uc.UserUsecase.DeleteUser(id)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success delete user", nil)
}

func (uc *UserController) LoginUser(c echo.Context) error {
	var request dto.LoginRequestDTO

	err := c.Bind(&request)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	resp, err := uc.UserUsecase.UserLogin(request)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "user login success", resp)
}

func (uc *UserController) DownloadUserImageProfile(c echo.Context) error {
	resp, err := uc.UserUsecase.DownloadUserImageProfile("john")
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "download user image profile success", resp)
}

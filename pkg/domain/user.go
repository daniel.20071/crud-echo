package domain

import (
	"mime/multipart"

	"github.com/kuma-coffee/crud-echo/pkg/dto"
)

type User struct {
	Id         int    `json:"id"`
	Username   string `json:"username"`
	Email      string `json:"email"`
	Address    string `json:"address"`
	Password   string `json:"password"`
	ImgProfile string `json:"img_profile"`
}

type UserRepository interface {
	GetUsers() ([]User, error)
	GetUserById(id int) (User, error)
	PostUser(user User) error
	UpdateUser(id int, user User) error
	DeleteUser(id int) error
	GetUserEmail(email string) (User, error)
}

type UserUsecase interface {
	GetUsers() ([]User, error)
	GetUserById(id int) (User, error)
	PostUser(userDTO dto.UserDTO, src multipart.File) error
	UpdateUser(id int, userDTO dto.UserDTO) error
	DeleteUser(id int) error
	GetUserEmail(email string) (User, error)
	UserLogin(req dto.LoginRequestDTO) (interface{}, error)
	UploadUserImageProfile(src multipart.File, username string) error
	DownloadUserImageProfile(username string) (string, error)
}

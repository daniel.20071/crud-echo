package dto

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type UserDTO struct {
	Username   string `json:"username"`
	Email      string `json:"email"`
	Address    string `json:"address"`
	Password   string `json:"password"`
	ImgProfile string `json:"img_profile"`
}

type LoginRequestDTO struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Address  string `json:"address"`
	Token    string `json:"token"`
}

func (u UserDTO) Validation() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Username, validation.Required),
		validation.Field(&u.Email, validation.Required, is.Email),
		validation.Field(&u.Address, validation.Required),
		validation.Field(&u.Password, validation.Required, validation.Length(6, 20)),
	)
}

package db

import (
	"context"
	"log"

	"cloud.google.com/go/storage"
	firebase "firebase.google.com/go"
	"github.com/kuma-coffee/crud-echo/config"
	"google.golang.org/api/option"
)

func NewFirebaseStorage(conf config.Configuration) *storage.BucketHandle {
	ctx := context.Background()
	opt := option.WithCredentialsFile("./config/firebase_service.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		panic(err)
	}
	client, err := app.Storage(ctx)
	if err != nil {
		log.Fatalln(err)
	}

	storeRef, err := client.Bucket(conf.ClientBucket)
	if err != nil {
		panic(err)
	}

	return storeRef

}
